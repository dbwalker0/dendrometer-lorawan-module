cls
set proj=pca-00014

:: I need to get rid of the extra files
del "*.drl,*"
del "*.rou,*"

if not exist %proj% (mkdir %proj%) else (del /Q %proj%\*.*)
if exist %proj%.zip del /Q %proj%.zip

set layers=%project:~-5,-4%
echo %layers%

copy TOP.art %proj%\%proj%.GTL
copy BOTTOM.art %proj%\%proj%.GBL
copy GPLANE.art %proj%\%proj%.G2L
copy PPLANE.art %proj%\%proj%.G3L
copy SILKSCREEN_TOP.art %proj%\%proj%.GTO
copy SILKSCREEN_BOTTOM.art %proj%\%proj%.GBO
copy SOLDERMASK_TOP.art %proj%\%proj%.GTS
copy SOlDERMASK_BOTTOM.art %proj%\%proj%.GBS
copy *.drl %proj%\%proj%_drill.XLN
rem copy *.rou %proj%\%proj%_route.XLN
copy Outline.art %proj%\%proj%.GKO

cd %proj%
tar -a -c -f ..\%proj%.zip *.*
cd ...
pause