
# LoRaWAN eDendro (prototype)#

This is the electrical repository for the LoRa version of the dendrometer. It contains an OrCAD schematic design project and a Cadence Allegro board layout. Unlike the previous version of the LoRa dendrometer (PCA-00013), this uses a module that will be easy to hand assemble so it should be useful for prototyping and getting some experience with LoRaWAN.

### What is this repository for? ###

* eDendro that uses a module suitable for hand assembly
* Revision A

### Who do I talk to? ###

* David Walker (david@eplant.bio)
* Other community or team contact